using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chicken : Bird
{
    public override void MakeSound()
    {
        Debug.Log("Chip chip..");
    }

    private void Fly()
    {
        Debug.Log("Chicken flying");
    }
}
