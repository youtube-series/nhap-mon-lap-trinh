using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : Animal
{
    public override void MakeSound()
    {
        //Debug.Log("Chip chip..");
    }

    protected override void Move()
    {
        base.Move();
        this.Fly();
    }

    private void Fly()
    {
        Debug.Log("Bird flying");
    }
}
