using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : Animal
{
    public override void MakeSound()
    {
        Debug.Log("Meow meow..");
        this.Move();
    }
}
