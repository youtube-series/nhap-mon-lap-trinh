using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    public Animal animal;

    private void FixedUpdate()
    {
        if(this.animal != null)
        {
            this.animal.MakeSound();
        }
    }
}
