using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : Animal
{
    public override void MakeSound()
    {
        Debug.Log("Gau gau..");
    }

    protected override void Move()
    {
        Vector3 target = transform.position;
        target.y += 2f;
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime);
    }
}
