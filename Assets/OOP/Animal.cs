using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        this.Move();
    }

    public virtual void MakeSound()
    {
        Debug.Log("What am I?");
    }

    protected virtual void Move()
    {
        Vector3 target = transform.position;
        target.y += 0.01f;
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime);
    }
}
