using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCtrl : MonoBehaviour
{
    public Despawner despawner;
    public EnemyDamageReceiver damageReveiver;

    private void Awake()
    {
        this.despawner = GetComponent<Despawner>();
        this.damageReveiver = GetComponent<EnemyDamageReceiver>();
    }
}
