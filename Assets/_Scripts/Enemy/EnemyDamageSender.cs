﻿using UnityEngine;
using System.Collections;

public class EnemyDamageSender : DamageSender
{
    [Header("Enemy")]
    public EnemyCtrl enemyCtrl; //Controller

    private void Awake()
    {
        this.enemyCtrl = GetComponent<EnemyCtrl>();
    }

    protected override void ColliderSendDamage(Collider2D collision)
    {
        base.ColliderSendDamage(collision);

        this.enemyCtrl.damageReveiver.Receive(1);
    }
}
